import React, { Component } from 'react'
import SliderComponent from '../../components/Slider/SliderComponent';
import CardComponent from '../../components/Card/CardComponent';
import FooterComponent from '../../components/Footer/FooterComponent';

class HomeComponent extends Component{
    constructor(props){
        super(props)
        this.state={

        }
       this.login=this.login.bind(this);
       this.signup=this.signup.bind(this);
    }
    login(){
        this.props.history.push('/login');
    }
    signup(){
        this.props.history.push('/signup');
    }
    render(){
        return(
            <div className="home">
                
                 <h2 className="text-center">The Workout Zone</h2>
                 <button className="success" style={{width:'100px'}} onClick={() => this.login()}>Login</button>
                 <br/><br/>
                 <button className="success" style={{width:'100px'}} onClick={() => this.signup()}>Signup</button>
                 <SliderComponent />
             <CardComponent 
             title="SK-GYM"
             subtitle="GOLD-Package"
             text="Here is a gold package for GYM lover in which you will get 3 month of
             membership period,
             where you will get 2 time massage and SONA and at minimum price of 3000 "
             />
            <FooterComponent/>
            </div>

        )
    }
}
export default HomeComponent;