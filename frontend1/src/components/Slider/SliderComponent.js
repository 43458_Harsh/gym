import React from "react";
import { UncontrolledCarousel } from "reactstrap";

//import img from Images/images.jpg

const items = [
  {
    src: "https://wallpaperaccess.com/full/1633729.jpg",
    altText: "Slide 1",
    caption: "NO PAIN NO GAIN",
    header: "SK-GYM",
    key: "1",
  },


  {
    src: "https://wallpaperaccess.com/full/1633734.jpg",
    altText: "Slide 2",
    caption: "NO PAIN NO GAIN",
    header: "SK-GYM",
    key: "2",
  },
];

const SliderComponent = () => <UncontrolledCarousel items={items} />;

export default SliderComponent;
