import React from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Button,
} from "reactstrap";
//import Images from './components/Images'


const CardComponent = (props) => {
  return (
       
       <div>
      <Card className="container col-sm-3">
        <CardImg top width="100%" src="https://i.pinimg.com/originals/96/22/ed/9622edec12fb5f8efe6c857e6ab3a4b2.jpg" alt="download" />
        <div class="cardbody">
        <CardBody>
            <CardTitle tag="h5">{props.title}</CardTitle>

          <CardText>{props.text}</CardText>
          <Button>Find In {props.title}</Button>
        </CardBody>
        </div>
      </Card>
    </div>
    

  );
};

export default CardComponent;
